# Configuration for Alacritty, the GPU enhanced terminal emulator.

env:
  # TERM variable
  #
  # This value is used to set the `$TERM` environment variable for
  # each instance of Alacritty. If it is not present, alacritty will
  # check the local terminfo database and use `alacritty` if it is
  # available, otherwise `xterm-256color` is used.
  TERM: alacritty

  # Values for `decorations`:
  #     - full: Borders and title bar
  #     - none: Neither borders nor title bar
  #
  # Values for `decorations` (macOS only):
  #     - transparent: Title bar, transparent background and title bar buttons
  #     - buttonless: Title bar, transparent background and no title bar buttons
  decorations: none

  # Window title
  title: Alacritty

  # Allow terminal applications to change Alacritty's window title.
  #dynamic_title: true

scrolling:
  # Maximum number of lines in the scrollback buffer.
  # Specifying '0' will disable scrolling.
  history: 20000

  # Scrolling distance multiplier.
  multiplier: 3

# Font configuration
font:
  normal:
    family: Source Code Pro Medium

  # Point size
  size: 9.0

# If `true`, bold text is drawn using the bright color variants.
#draw_bold_text_with_bright_colors: false

colors:
  primary:
    background: '#181818'
    foreground: '#d6d6d6'

    # Bright and dim foreground colors
    #
    # The dimmed foreground color is calculated automatically if it is not
    # present. If the bright foreground color is not set, or
    # `draw_bold_text_with_bright_colors` is `false`, the normal foreground
    # color will be used.
    dim_foreground: '#0c0c0c'
    bright_foreground: '#ffffff'

  # Cursor colors
  #
  # Colors which should be used to draw the terminal cursor.
  #
  # Allowed values are CellForeground/CellBackground, which reference the
  # affected cell, or hexadecimal colors like #ff00ff.
  cursor:
    text: CellBackground
    cursor: CellForeground

  # Selection colors
  #
  # Colors which should be used to draw the selection area.
  #
  # Allowed values are CellForeground/CellBackground, which reference the
  # affected cell, or hexadecimal colors like #ff00ff.
  selection:
    text: CellBackground
    background: CellForeground

  bright:
    red:     '#ff6662'
    yellow:  '#fcf571'
    magenta: '#ff9bcf'
    blue:    '#70cffd'
    cyan:    '#05f5f5'
    green:   '#60fe9b'
    white:   '#ffffff'
    black:   '#808080'

  normal:
    red:     '#f20c04'
    yellow:  '#fff105'
    magenta: '#ff3da3'
    blue:    '#16b1ff'
    cyan:    '#2aeeee'
    green:   '#00ff5e'
    white:   '#d6d6d6'
    black:   '#181818'

  dim:
    red:     '#950400'
    yellow:  '#9e824c'
    magenta: '#fd0085'
    blue:    '#0087c9'
    cyan:    '#00caca'
    green:   '#00cd4c'
    black:   '#020202'
    white:   '#0c0c0c'

cursor:
  style:
    # Cursor shape
    #
    # Values for `shape`:
    #   - ▇ Block
    #   - _ Underline
    #   - | Beam
    shape: Block

    # Cursor blinking state
    #
    # Values for `blinking`:
    #   - Never: Prevent the cursor from ever blinking
    #   - Off: Disable blinking by default
    #   - On: Enable blinking by default
    #   - Always: Force the cursor to always blink
    blinking: Always

# Live config reload (changes require restart)
live_config_reload: true

key_bindings:
  - { key: Paste,                                       action: Paste          }
  - { key: Copy,                                        action: Copy           }
  - { key: L,         mods: Control,                    action: ClearLogNotice }
  - { key: L,         mods: Control, mode: ~Vi|~Search, chars: "\x0c"          }
  - { key: PageUp,                                      action: ScrollPageUp,  }
  - { key: PageDown,                                    action: ScrollPageDown }
  - { key: Home,      mods: Shift,   mode: ~Alt,        action: ScrollToTop,   }
  - { key: End,       mods: Shift,   mode: ~Alt,        action: ScrollToBottom }

  # (Windows, Linux, and BSD only)
  - { key: V,              mods: Control|Shift, mode: ~Vi,        action: Paste            }
  - { key: C,              mods: Control|Shift,                   action: Copy             }
  - { key: F,              mods: Control|Shift, mode: ~Search,    action: SearchForward    }
  - { key: B,              mods: Control|Shift, mode: ~Search,    action: SearchBackward   }
  - { key: C,              mods: Control|Shift, mode: Vi|~Search, action: ClearSelection   }
  - { key: Insert,         mods: Shift,                           action: PasteSelection   }
  - { key: Key0,           mods: Control,                         action: ResetFontSize    }
  - { key: Equals,         mods: Control,                         action: IncreaseFontSize }
  - { key: Plus,           mods: Control,                         action: IncreaseFontSize }
  - { key: NumpadAdd,      mods: Control,                         action: IncreaseFontSize }
  - { key: Minus,          mods: Control,                         action: DecreaseFontSize }
  - { key: NumpadSubtract, mods: Control,                         action: DecreaseFontSize }

